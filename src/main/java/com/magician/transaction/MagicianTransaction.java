package com.magician.transaction;

import com.magician.transaction.cache.TransactionCacheManager;
import com.magician.transaction.proxy.TransactionProxy;

/**
 * 事务管理
 */
public class MagicianTransaction {

    /**
     * 获取带事务控制的代理对象
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> T getProxyObject(Class<T> cls){
        /* 先从缓存取，如果有就直接返回 */
        Object object = TransactionCacheManager.get(cls);
        if(object != null){
           return (T)object;
        }

        /* 如果缓存里没有，就创建一个 */
        TransactionProxy transactionProxy = new TransactionProxy();
        object = transactionProxy.getProxy(cls);
        TransactionCacheManager.add(cls, object);

        return (T)object;
    }
}
