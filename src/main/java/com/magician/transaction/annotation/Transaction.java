package com.magician.transaction.annotation;

import com.magician.jdbc.core.constant.enums.TractionLevel;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Transaction {

    /**
     * 事务个例级别
     * @return
     */
    TractionLevel level() default TractionLevel.READ_COMMITTED;
}
