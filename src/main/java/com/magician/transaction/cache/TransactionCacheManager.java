package com.magician.transaction.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 因为事务控制用的是动态代理，为了避免每次获取对象的收 都临时生成，
 * 所以用一个缓存将生成的对象缓存下来，这样下次直接取就好了，不用每次都生成，
 */
public class TransactionCacheManager {

    private static Map<String, Object> objectCacheMap = new ConcurrentHashMap<>();

    /**
     * 添加缓存
     * @param cls
     * @param object
     */
    public static void add(Class cls, Object object){
        objectCacheMap.put(cls.getName(), object);
    }

    /**
     * 获取缓存
     * @param cls
     * @return
     */
    public static Object get(Class cls){
        return objectCacheMap.get(cls.getName());
    }
}
