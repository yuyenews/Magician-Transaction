<br/>

<div align=center>
<img width="260px;" src="http://magician-io.com/img/logo-white.png"/>
</div>

<br/>

<div align=center>

<img src="https://img.shields.io/badge/licenes-MIT-brightgreen.svg"/>
<img src="https://img.shields.io/badge/jdk-11+-brightgreen.svg"/>
<img src="https://img.shields.io/badge/maven-3.5.4+-brightgreen.svg"/>
<img src="https://img.shields.io/badge/release-master-brightgreen.svg"/>

</div>
<br/>

<div align=center>
Magician's official transaction management component
</div>


## Introduction

Magician-Transaction is the official transaction management component of Magician, which completes transaction monitoring in the form of annotated statements

## installation steps

### 1. Import dependencies

Introduce the following dependencies in projects that have already used Magician-JDBC

```xml
<dependency>
    <groupId>com.github.yuyenews</groupId>
    <artifactId>Magician-Transaction</artifactId>
    <version>last version</version>
</dependency>
```

### 2. Add this annotation to the method that requires transaction monitoring

```java
public class DemoService {
	
	// Use the default isolation level
	@Transaction
	public String save(){
		return "ok";
	}
	
	// Custom isolation level
	@Transaction(level = TractionLevel.READ_COMMITTED)
	public String update(){
		return "ok";
	}

}
```

### 3. Get proxy object
If you want the annotation to take effect, you must obtain the proxy object, and you cannot directly manipulate the new object
```java
DemoService demoService = MagicianTransaction.getProxyObject(DemoService.class);
```